# -*- coding: utf-8 -*-
from os import environ

import discord
import sentry_sdk
from discord.ext import commands

import logutils
import repository
from emote_creation import saveImageToDisk

sentry_sdk.init(dsn=environ.get('SENTRY_DSN_BOTCHAN', ''))
pool = repository.setupDbPool()
logger = logutils.setupLogging()


def setupBot():
    bot = commands.Bot(command_prefix='!', description='Botchan can be used to create and post images and gifs')

    @bot.command()
    async def ping(ctx):
        await ctx.send('pong')

    @bot.command()
    async def list(ctx):
        emotes = repository.getAllEmotes(pool)
        await ctx.send(emotes)

    @bot.command()
    async def create(ctx, keyword):
        url = ctx.message.attachments.url
        logger.debug(f'Creating emote with keyword {keyword}')
        imgFilePath = saveImageToDisk(url, logger)
        if imgFilePath:
            result = repository.insertNewEmote(keyword, imgFilePath, pool)
            if not result:
                await ctx.send(f'Error inserting emote into db')
            else:
                await ctx.send(f'New emote created with keyword {keyword}')
        else:
            await ctx.send(f'Error downloading remote image from url {url}')

    @bot.command()
    async def show(ctx, keyword):
        logger.debug(f'Searching for emote with keyword: {keyword}')
        emote = repository.findEmoteByKeyword(keyword, pool)
        if emote:
            logger.debug(f'Serving emote with keyword: {keyword}')
            with open(emote['path'], 'rb') as fp:
                await ctx.send(file=discord.File(fp))
        else:
            await ctx.send(f'Could not find emote with keyword: {keyword}')

    return bot


def main():
    bot = setupBot()
    logger.info("Starting bot")
    try:
        bot.run(environ['DISCORD_API_TOKEN'])
    except Exception as e:
        logger.exception("Unexpected error in main loop", exc_info=e)


if __name__ == '__main__':
    main()
