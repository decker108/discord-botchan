create database botchan_db;
\c botchan_db
create role botchan_db_user with password 'secret';
grant connect on DATABASE botchan_db to botchan_db_user;
ALTER ROLE botchan_db_user WITH LOGIN;

create table emotes(
  id serial primary key not null,
  keyword varchar(32) not null unique,
  path varchar(256) not null unique,
  created_date timestamp with time zone default now()
);
GRANT SELECT,INSERT,UPDATE,DELETE ON emotes TO botchan_db_user;
GRANT USAGE,SELECT,UPDATE ON emotes_id_seq TO botchan_db_user;
