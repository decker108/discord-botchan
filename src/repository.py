# -*- coding: utf-8 -*-
from os import environ
import logging
from configparser import ConfigParser
import psycopg2.pool
from psycopg2.extras import DictCursor


def setupDbPool():
    cfgParser = ConfigParser()
    cfgParser.read(environ['BOTCHAN_DB_CONFIG'])
    min_conn = cfgParser.get('root', 'db.min_conn')
    max_conn = cfgParser.get('root', 'db.max_conn')
    addr = cfgParser.get('root', 'db.addr')
    portnum = cfgParser.get('root', 'db.port')
    db_name = cfgParser.get('root', 'db.name')
    username = cfgParser.get('root', 'db.username')
    passw = environ['BOTCHAN_DB_PASSWORD']
    return psycopg2.pool.SimpleConnectionPool(minconn=min_conn, maxconn=max_conn,
                                              database=db_name, host=addr, user=username, password=passw, port=portnum,
                                              cursor_factory=DictCursor)


def findEmoteByKeyword(keyword, pool):  # TODO replace db calls with aiopg
    result = conn = None
    try:
        conn = pool.getconn()
        with conn.cursor() as cursor:
            cursor.execute("SELECT path FROM emotes WHERE keyword = %s LIMIT 1", (keyword,))
            if cursor.rowcount > 0:
                result = cursor.fetchone()
            else:
                logging.getLogger("discord").warning("No emotes found for keyword", keyword)
    except Exception as e:
        logging.getLogger("discord").error(e)
    finally:
        pool.putconn(conn)
    return result


def insertNewEmote(keyword, filePath, pool):  # TODO replace db calls with aiopg
    conn = None
    try:
        conn = pool.getconn()
        with conn.cursor() as cursor:
            cursor.execute("INSERT INTO emotes(keyword, path) VALUES (%s, %s)", (keyword, filePath))
            conn.commit()
            return True
    except Exception as e:
        logging.getLogger("discord").error(e)
    finally:
        pool.putconn(conn)


def getAllEmotes(pool):  # TODO replace db calls with aiopg
    conn = result = None
    try:
        conn = pool.getconn()
        with conn.cursor() as cursor:
            cursor.execute("SELECT keyword FROM emotes")
            if cursor.rowcount > 0:
                result = ', '.join([item['keyword'] for item in cursor.fetchall()])
            else:
                result = 'No emotes found...'
    except Exception as e:
        logging.getLogger("discord").error(e)
    finally:
        pool.putconn(conn)
    return result
