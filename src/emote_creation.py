# -*- coding: utf-8 -*-
from os import path
import shutil
from urllib3.util import url
import requests
import uuid

commonImgFormats = ['.png', '.jpg', '.jpeg', '.gif', '.bmp', '.tiff', '.tif']


def _hasImageExtension(imgFilePath):
    return imgFilePath[imgFilePath.find('.'):] in commonImgFormats


def _extractFileName(imgUrl):
    urlPath = url.parse_url(imgUrl).path
    return urlPath if '/' not in urlPath else urlPath[urlPath.rfind('/')+1:]


def saveImageToDisk(imgUrl, logger):
    with requests.get(imgUrl, stream=True) as r:  # TODO replace with aiohttp client
        if r.status_code == 200:
            urlPathPart = _extractFileName(imgUrl)
            if not urlPathPart or urlPathPart == '/' or not _hasImageExtension(urlPathPart):
                fileName = str(uuid.uuid4()) + '.png'
            else:
                fileName = urlPathPart
            filePath = path.expanduser('~/discord/' + fileName)

            with open(filePath, 'wb') as f:  # TODO replace with aiofiles
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)
            return fileName
        else:
            logger.error(f"Non-OK response received for {imgUrl}")
