# -*- coding: utf-8 -*-
import logging
from logging.handlers import RotatingFileHandler
from os import environ


def setupLogging():
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    discordLogger = logging.getLogger("botchan")
    discordLogger.setLevel(logging.DEBUG)

    logfilePath = environ.get("BOTCHAN_LOG_PATH", "/var/log/botchan/app.log")
    rfh = RotatingFileHandler(logfilePath, maxBytes=1000000, backupCount=2)
    rfh.setFormatter(formatter)

    strh = logging.StreamHandler()
    strh.setFormatter(formatter)

    discordLogger.addHandler(rfh)
    discordLogger.addHandler(strh)
    return discordLogger
